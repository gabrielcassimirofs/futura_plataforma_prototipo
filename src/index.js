import { CssBaseline } from "@material-ui/core";
import { StyledEngineProvider } from "@mui/material";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import App from "./App";
import "./reset.css";

ReactDOM.render(
  <StyledEngineProvider injectFirst>
    <CssBaseline>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </CssBaseline>
  </StyledEngineProvider>,
  document.getElementById("root")
);
