import React, { useEffect, useState } from "react";
import CustomAppBar from "./components/AppBar";
import { createTheme, MuiThemeProvider } from "@material-ui/core";
import { colors } from "@material-ui/core";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./pages/Home";
import TerminaisPage from "./pages/Terminais";
import LandingPage from "./pages/LandingPage";

const theme = createTheme({
  primary: {
    dark: colors.lightBlue["A900"],
    main: "#2a6ea0",
    light: colors.lightBlue["A100"],
  },
  secondary: {
    dark: colors.orange["A700"],
    main: "#EF7E10",
    light: colors.orange["A200"],
  },
  companySecondary: {
    dark: colors.orange["A700"],
    main: colors.orange["A700"],
    light: colors.orange["A200"],
  },
  palette: {
    warning: {
      dark: colors.orange["A700"],
      main: "#EF7E10",
      light: colors.orange["A200"],
    },
  },
});

function App() {
  const [path, setPath] = useState("");

  return (
    <MuiThemeProvider theme={theme}>
      <CustomAppBar path={path} />
      <div>
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="/home" element={<HomePage setPath={setPath} />} />
          <Route path="terminais" element={<TerminaisPage setPath={setPath} />} />
        </Routes>
      </div>
    </MuiThemeProvider>
  );
}

export default App;
