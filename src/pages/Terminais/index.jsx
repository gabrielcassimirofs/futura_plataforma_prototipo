import React, { useEffect, useState } from "react";
import InfoWidget from "./components/InfoWidget";
import terminaisStyles from "./styles";
import { BarChart, FiberManualRecord } from "@material-ui/icons";
import Company from "./components/Company";
import TerminalItem from "./components/TerminalItem";
import TerminalPopover from "./components/TerminalPopover";
import { useLocation } from "react-router-dom";
import { getTerminais } from "../../api/api";
import PopupAlert from "../../components/Popup";

const TerminaisPage = ({ setPath }) => {
  const classes = terminaisStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const [terminais, setTerminais] = useState([]);
  const [companySelected, setCompanySelected] = useState(0);
  const [open, setOpen] = useState(false);
  const [openPopup, setOpenPopup] = useState(false);

  const { pathname } = useLocation();

  useEffect(() => {
    setPath(pathname);
    setTerminais(getTerminais());
  }, []);

  return (
    <div className={classes.root}>
      <div className={classes.titlePage}>Controle de Terminais</div>
      <div className={classes.panel}>
        <div className={classes.sideMenu}>
          <div className={classes.sideMenuButtonSelected}>
            <div className={classes.sideMenuText}>Terminais</div>
          </div>
          <div className={classes.sideMenuButton}>
            <div className={classes.sideMenuText}>SAT</div>
          </div>
          <div className={classes.sideMenuButton}>
            <div className={classes.sideMenuText}>TEF</div>
          </div>
          <div className={classes.sideMenuButton}>
            <div className={classes.sideMenuText}>Balança</div>
          </div>
        </div>
        <div className={classes.lineDivider}></div>
        <div className={classes.panelTerminal}>
          <div className={classes.infoArea}>
            <div>
              <div className={classes.panelTitle}>
                Totalizadores <BarChart style={{ color: "#2C699D", height: 20, marginTop: "auto", marginBottom: "auto" }} />
              </div>

              <div style={{ display: "flex" }}>
                <InfoWidget title="Terminais" value={11} subtitle="Todos" />
                <div className={classes.verticalDivider} />
                <InfoWidget title="Caixas" value={6} subtitle="Ativos" />
                <div className={classes.verticalDivider} />
                <InfoWidget title="Pré-Vendas" value={5} subtitle="Ativos" />
                <div className={classes.verticalDivider} />
              </div>
            </div>
            <div style={{ borderLeft: "2px solid rgba(196, 196, 196, 0.3)" }}>
              <div className={classes.panelTitle}>
                Status <FiberManualRecord style={{ color: "#2C699D", height: 16, marginTop: "auto", marginBottom: "auto" }} />
              </div>
              <div style={{ display: "flex" }}>
                <InfoWidget title="Em Operação" value={11} subtitle="Movimentação" />
                <div className={classes.verticalDivider} />
                <InfoWidget title="Inoperante" value={2} subtitle="Sem Movimentação" />
                <div className={classes.verticalDivider} />
                <InfoWidget title="Desativado" value={1} subtitle="Cancelado" />
              </div>
            </div>
          </div>
          <div className={classes.panelArea}>
            <div className={classes.panelTitle}>Empresas:</div>
            <div style={{ display: "flex" }}>
              <div className={companySelected == 0 ? classes.buttonSelected : classes.button} onClick={() => setCompanySelected(0)}>
                <div style={{ marginTop: 2 }}>Todas</div>
              </div>
              <div className={classes.panelAreaItems}>
                <Company isSelected={companySelected == 1} setCompany={setCompanySelected} number={1} />
                <Company isSelected={companySelected == 2} setCompany={setCompanySelected} number={2} />
                <Company isSelected={companySelected == 3} setCompany={setCompanySelected} number={3} />
                <Company isSelected={companySelected == 4} setCompany={setCompanySelected} number={4} />
                <Company isSelected={companySelected == 5} setCompany={setCompanySelected} number={5} />
              </div>
            </div>
          </div>
          <div className={classes.panelArea}>
            <div className={classes.panelTitle}>Terminais:</div>
            <div className={classes.panelAreaItems}>
              {companySelected == 0 &&
                terminais.map((value) => <TerminalItem text={value.name} value={value.number} active={value.isActive} mobile={value.isMobile} anchor={setAnchorEl} setOpen={setOpen} />)}
              {companySelected != 0 &&
                terminais
                  .filter((x) => x.parent == companySelected)
                  .map((value) => <TerminalItem text={value.name} value={value.number} active={value.isActive} mobile={value.isMobile} anchor={setAnchorEl} setOpen={setOpen} />)}
            </div>
          </div>
        </div>
      </div>
      <TerminalPopover open={open} setOpen={setOpen} anchor={anchorEl} setOpenPopup={setOpenPopup} />
      <PopupAlert open={openPopup} handleClose={() => setOpenPopup(false)} />
    </div>
  );
};

export default TerminaisPage;
