import React from "react";
import companyStyles from "./styles";

const Company = ({ setCompany, number, isSelected }) => {
  const classes = companyStyles();
  return (
    <div className={isSelected ? classes.rootSelected : classes.root} onClick={() => setCompany(number)}>
      <div className={classes.text}>
        <b>ID: 000001 </b>
        <br />
        08.366.892/0001-02
        <br />
        Empresa Teste do brasil Ltda <br />
        <b>Nome Fantasia:</b> Empresa teste
      </div>
    </div>
  );
};

export default Company;
