import { makeStyles } from "@material-ui/core";

const companyStyles = makeStyles((theme) => ({
  root: {
    width: 170,
    height: 61,
    background: "#FFFFFF",
    border: "0.5px solid #2E699D",
    borderTop: "3px solid #2E699D",
    boxSizing: "border-box",
    borderRadius: "0px 0px 3px 3px",
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
    cursor: "pointer",
  },
  rootSelected: {
    width: 170,
    height: 61,
    background: "#DDEAF5",
    border: "0.5px solid #2E699D",
    borderTop: "3px solid #2E699D",
    boxSizing: "border-box",
    borderRadius: "0px 0px 3px 3px",
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
    cursor: "pointer",
  },
  text: {
    fontSize: 9,
    margin: "auto",
  },
}));

export default companyStyles;
