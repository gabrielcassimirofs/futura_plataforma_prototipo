import React from "react";
import infoWidgetStyles from "./styles";

const InfoWidget = ({ title, value, subtitle }) => {
  const classes = infoWidgetStyles();
  return (
    <div className={classes.root}>
      <div className={classes.title}>
        {title || ""}: {value || ""}
      </div>
      <div className={classes.subtitle}>{subtitle || ""}</div>
    </div>
  );
};

export default InfoWidget;
