import { makeStyles } from "@material-ui/core";

const infoWidgetStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-evenly",
    width: "auto",
    height: 50,
    marginLeft: 5,
    marginRight: 5,
  },
  title: {
    color: "#2C699D",
    fontWeight: "bold",
    fontSize: "0.9vw",
    display: "block",
    whiteSpace: "nowrap",
    width: "90%",
  },
  subtitle: {
    color: "#9F9F9F",
    display: "block",
    whiteSpace: "nowrap",
    fontSize: "0.8vw",
  },
}));

export default infoWidgetStyles;
