import { makeStyles } from "@material-ui/core";

const popoverTerminalStyles = makeStyles((theme) => ({
  header: {
    display: "flex",
    justifyContent: "space-between",
    height: 17,
    width: "100%",
    backgroundColor: "#DDEAF5",
    fontSize: 10,
  },
  text: {
    display: "flex",
    justifyContent: "center",
    height: "100%",
    marginTop: 2,
    marginLeft: 5,
  },
  textButton: {
    height: 17,
    cursor: "pointer",
  },
  content: {
    display: "flex",
    margin: 9,
    color: "#7D7D7D",
    fontSize: 8,
    height: 140,
    width: "100%",
  },
  button: {
    width: 91,
    height: 17,
    background: "#2E699D",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 4,
    color: "#FFFFFF",
    fontWeight: "bold",
    display: "flex",
    justifyContent: "center",
    fontSize: 12,
    margin: "auto",
    marginTop: 10,
    marginBottom: 10,
    cursor: "pointer",
  },
  textCustom: {
    color: (props) => (props.active ? "#19950f" : "#85020B"),
  },
  verticalDivider: {
    border: "1px solid rgba(196, 196, 196, 0.3)",
    height: "80%",
    width: 0,
    marginLeft: 15,
    marginRight: 15,
  },
}));

export default popoverTerminalStyles;
