import { IconButton, Popover } from "@material-ui/core";
import React, { useEffect, useRef, useState } from "react";
import popoverTerminalStyles from "./styles";
import { Add, Remove } from "@material-ui/icons";

const TerminalPopover = ({ open, setOpen, anchor, active, setOpenPopup }) => {
  const classes = popoverTerminalStyles({ active });
  const [anchorEl, setAnchorEl] = useState(null);
  const [size, setSize] = useState(145);
  const popoverRef = useRef(null);

  useEffect(() => {
    if (open) {
      setAnchorEl(anchor);
    }
  }, [open]);

  return (
    <div>
      <Popover
        ref={popoverRef}
        open={open}
        onClose={() => setOpen(false)}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        PaperProps={{
          style: {
            width: size,
            height: 177,
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "start",
            overflowY: "auto",
            "&::-webkit-scrollbar": {
              width: "6px",
              height: "6px",
            },
            "&::-webkit-scrollbar-thumb": {
              backgroundColor: "rgba(0, 0, 0, 0.2)",
            },
          },
        }}
      >
        <div className={classes.header}>
          <div className={classes.text}>Edição</div>
          {size == 145 ? (
            <Add
              className={classes.textButton}
              onClick={() => {
                setSize(300);
              }}
            />
          ) : (
            <Remove
              className={classes.textButton}
              onClick={() => {
                setSize(145);
              }}
            />
          )}
        </div>
        <div className={classes.content}>
          <div>
            <b style={{ color: "#000000" }}>Código de Indentificação:</b>
            <br />
            123123123123123123
            <div className={classes.button} onClick={() => setOpenPopup(true)}>
              Status
            </div>
            <b style={{ color: "#000000" }}>Última edição:</b>
            <br />
            12/04/2022 11:20
            <br />
            Usuário: Caroline
            <br />
            <div style={{ display: "flex" }}>
              Status Atual: <div className={classes.textCustom}>{active ? "Em Operação" : "Inoperante"}</div>
            </div>
          </div>
          {size == 300 && (
            <>
              <div className={classes.verticalDivider} />
              <div>
                <b style={{ color: "#000000" }}>SAT:</b>
                <br />
                <div style={{ display: "flex" }}>
                  Dimep: <div style={{ color: "#19950f" }}>(Ativo)</div>
                </div>
                <div style={{ display: "flex" }}>
                  Kriptos: <div style={{ color: "#85020B" }}>(Desativado)</div>
                </div>
                <b style={{ color: "#000000" }}>Balanças:</b>
                <br />
                <div style={{ display: "flex" }}>
                  {"UPX Solution - Wind(C6 MT):"} <div style={{ color: "#19950f" }}>(Ativo)</div>
                </div>
                <div style={{ display: "flex" }}>
                  Elgin <div style={{ color: "#19950f" }}>(Ativo)</div>
                </div>
                <br />
                <b style={{ color: "#000000" }}>TEF:</b>
                <div style={{ display: "flex" }}>
                  TEF Futura <div style={{ color: "#19950f" }}>(Ativo)</div>
                </div>
                <br />
                <div style={{ display: "flex" }}>
                  <b style={{ color: "#000000" }}>{"NFC-e"}</b> <div style={{ color: "#19950f" }}>(Ativo)</div>
                </div>
              </div>
            </>
          )}
        </div>
      </Popover>
    </div>
  );
};

export default TerminalPopover;
