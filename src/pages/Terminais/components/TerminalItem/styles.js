import { makeStyles } from "@material-ui/core";

const terminalStyles = makeStyles((theme) => ({
  root: {
    width: 131,
    height: 61,
    background: "#FFFFFF",
    border: "0.5px solid #EF7E10",
    borderTop: "3px solid #EF7E10",
    boxSizing: "border-box",
    borderRadius: "0px 0px 3px 3px",
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    display: "flex",
    justifyContent: "space-between",
    cursor: "pointer",
  },
  textArea: {
    height: "100%",
    width: 85,
    marginTop: 15,
    marginLeft: 5,
  },
  text: {
    fontSize: 10,
    margin: "auto",
  },
  textWithColor: {
    fontSize: 9,
    margin: "auto",
    color: (props) => (props.active ? "#19950f" : "#85020B"),
  },
  image: {
    height: 25,
    marginTop: 15,
  },
}));

export default terminalStyles;
