import React from "react";
import terminalStyles from "./styles";
import { MoreVert } from "@material-ui/icons";
import { IconButton } from "@material-ui/core";

const TerminalItem = ({ text, value, mobile, active, anchor, setOpen }) => {
  const classes = terminalStyles({ active });
  return (
    <div className={classes.root}>
      <div className={classes.textArea}>
        <div className={classes.text}>
          {text || ""} <b>{value || ""}</b>
          <div className={classes.textWithColor}>{active ? "Em Operação" : "Inoperante"}</div>
        </div>
      </div>
      <img src={mobile ? "/images/android.png" : "/images/windows.png"} alt="" className={classes.image} />
      <IconButton
        style={{
          width: 20,
          height: 20,
        }}
        onClick={(e) => {
          anchor(e.currentTarget);
          setOpen(true);
        }}
      >
        <MoreVert
          style={{
            width: 20,
            height: 20,
          }}
        />
      </IconButton>
    </div>
  );
};

export default TerminalItem;
