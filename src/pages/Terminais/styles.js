import { makeStyles } from "@material-ui/core";

const terminaisStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    height: "calc(100vh - 110px)",
  },
  titlePage: {
    fontWeight: 500,
    fontSize: 18,
    width: "calc(100vw - 375px)",
    marginTop: 15,
    marginLeft: "auto",
    marginRight: "auto",
  },
  panel: {
    display: "flex",
    height: "calc(100vh - 175px)",
    width: "calc(100vw - 375px)",
    background: "#EEF4FA",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 4,
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: 15,
    justifyContent: "space-between",
    overflowY: "auto",
    "&::-webkit-scrollbar": {
      width: "6px",
      height: "6px",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "rgba(0, 0, 0, 0.2)",
    },
  },
  lineDivider: {
    height: "90%",
    width: 0,
    border: "1px solid rgba(0, 0, 0, 0.2)",
    marginTop: "auto",
    marginBottom: "auto",
    marginRight: 10,
  },
  sideMenu: {
    display: "flex",
    flexDirection: "column",
    height: "90%",
    width: 130,
    marginTop: "auto",
    marginBottom: "auto",
  },
  sideMenuButton: {
    display: "flex",
    justifyContent: "center",
    height: 25,
    marginBottom: 5,
    "&:hover": {
      color: "#2E699D",
      background: "#DDEAF5",
      boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
      borderRadius: 3,
      fontWeight: 600,
    },
    cursor: "pointer",
  },
  sideMenuButtonSelected: {
    display: "flex",
    justifyContent: "center",
    height: 25,
    marginBottom: 5,
    color: "#2E699D",
    background: "#DDEAF5",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 3,
    fontWeight: 600,
    cursor: "pointer",
  },
  sideMenuText: {
    margin: "auto",
    display: "block",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    width: "80%",
    cursor: "pointer",
  },
  panelTitle: {
    fontWeight: 600,
    fontStyle: "normal",
    fontSize: 16,
    display: "flex",
    justifyContent: "start",
    marginBottom: "auto",
    width: "auto",
  },
  panelTerminal: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
    height: "90%",
    margin: "auto",
  },
  infoArea: {
    display: "flex",
    justifyContent: "space-between",
    width: "90%",
    borderBottom: "2px solid rgba(196, 196, 196, 0.3)",
    backdropFilter: "blur(4px)",
  },
  verticalDivider: {
    border: "1px solid rgba(196, 196, 196, 0.3)",
    height: 40,
    width: 0,
  },
  panelArea: {
    width: "calc(100vw - 520px)",
    marginTop: 5,
  },
  panelAreaItems: {
    display: "flex",
    justifyContent: "flex-start",
    flexWrap: "wrap",
  },
  button: {
    width: 93,
    height: 26,
    marginTop: 5,
    display: "flex",
    justifyContent: "center",
    textAlign: "center",
    backgroundColor: "#FFFFFF",
    border: "0.5px solid #2E699D",
    boxSizing: "border-box",
    backdropFilter: "blur(4px)",
    borderRadius: 3,
    cursor: "pointer",
  },
  buttonSelected: {
    width: 93,
    height: 26,
    marginTop: 5,
    display: "flex",
    justifyContent: "center",
    textAlign: "center",
    backgroundColor: "#DDEAF5",
    border: "0.5px solid #2E699D",
    boxSizing: "border-box",
    backdropFilter: "blur(4px)",
    borderRadius: 3,
    cursor: "pointer",
  },
}));

export default terminaisStyles;
