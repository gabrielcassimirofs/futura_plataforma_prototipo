import React from "react";
import landingStyles from "./styles";

const LandingPage = () => {
  const classes = landingStyles();
  return (
    <div className={classes.root}>
      <div className={classes.bannerDiv}>
        <div className={classes.banner}>
          <div>
            <h5>
              O Futura Plataforma
              <br /> é a Gestão Online que <b>descomplica</b> o <b>seu negócio</b>
            </h5>
            <p>Um ERP que facilita a emissão de notas fiscais, controle de estoque além de facilitar a getsão do seu checkout.</p>
            <button>Comece agora</button>
          </div>
          <div className={classes.learnMore}>
            <p>
              planos a partir de:
              <br />
              R$ <b>50,00</b>
              <h4>{"saiba mais >"}</h4>
            </p>
          </div>
        </div>
      </div>
      <div className={classes.content}>
        <img src="/images/peopleimage.png" alt="" className={classes.peopleImage} />
        <div>
          <h5>
            Emissão de <b>nota fiscal</b> pode ser mais simples do que você imagina
          </h5>
          <p>
            Com o Futura Plataforma, você tem mais rapidez para emitir os três principais tipos de notas fiscais (NFe, NFSe e NFCe), não percisa informar os dados mais do que uma vez, os impostos são
            calculados automaticamente e muito mais!
          </p>
          <button>Experimente grátis</button>
        </div>
      </div>
    </div>
  );
};

export default LandingPage;
