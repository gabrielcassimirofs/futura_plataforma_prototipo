import { makeStyles } from "@material-ui/core";
import { Autorenew } from "@material-ui/icons";

const landingStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    height: "calc(100vh - 110px)",
    overflowY: "auto",
    "&::-webkit-scrollbar": {
      width: "6px",
      height: "6px",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "rgba(0, 0, 0, 0.2)",
    },
  },
  bannerDiv: {
    display: "flex",
    color: "#FFFFFF",
    alignItems: "center",
    width: "100%",
    maxWidth: "980px",
    margin: "0 auto",
    "& h5": {
      fontWeight: "bold",
      fontSize: 32,
      width: "60%",
      marginTop: 15,
    },
    "& b": {
      backgroundColor: "#F07E0F",
    },
    "& p": {
      fontSize: 14,
      width: "50%",
      marginTop: 15,
    },
    "& button": {
      border: 0,
      borderRadius: 20,
      fontSize: 16,
      fontWeight: "bold",
      color: "#FFFFFF",
      backgroundColor: "#EF7E10",
      height: 32,
      padding: "0px 20px 0px 20px ",
      marginTop: 15,
      cursor: "pointer",
    },
  },
  banner: {
    display: "flex",
    alignItems: "flex-start",
    margin: "0px  auto",
    width: "100%",
    padding: "48px",
    backgroundImage: "url(/images/rocketBackground.png)",
    backgroundPosition: "right 0",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
  },
  learnMore: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EF7E10",
    borderRadius: "100%",
    height: "150px",
    width: "150px",
    "& > p": {
      textAlign: "center",
      fontSize: "12px",
      lineHeight: "12px",
      display: "block",
      whiteSpace: "nowrap",
      margin: 0,
      width: "100%",
    },
    "& b": {
      fontSize: 32,
      lineHeight: "32px",
    },
    "& h4": {
      fontSize: 12,
      margin: "0 !important",
      display: "block",
      whiteSpace: "nowrap",
      textDecoration: "underline",
      cursor: "pointer",
    },
  },
  content: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    maxWidth: "980px",
    margin: "auto",
    color: "#4C7FAC",
    fontSize: 34,
    "& h5": {
      fontWeight: "bolder",
      marginTop: 15,
    },
    "& b": {
      color: "#FFFFFF",
      backgroundColor: "#F07E0F",
    },
    "& p": {
      marginTop: 15,
      fontSize: 14,
    },
    "& button": {
      border: 0,
      borderRadius: 20,
      fontWeight: "bold",
      color: "#FFFFFF",
      backgroundColor: "#EF7E10",
      height: 32,
      padding: "0px 20px 0px 20px ",
      marginTop: 15,
      cursor: "pointer",
      fontSize: 14,
    },
  },
  peopleImage: {
    width: "fit-content",
    maxWidth: "500px",
    padding: "48px 48px 48px 0px",
  },
}));

export default landingStyles;
