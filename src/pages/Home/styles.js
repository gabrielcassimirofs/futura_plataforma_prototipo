import { makeStyles } from "@material-ui/core";

const homeStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    height: "calc(100vh - 110px)",
  },
  panel: {
    display: "flex",
    width: "calc(100vw - 375px)",
    height: "calc(100vh - 155px)",
    background: "#EEF4FA",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 4,
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: 30,
    justifyContent: "space-between",
    overflowY: "auto",
    "&::-webkit-scrollbar": {
      width: "6px",
      height: "6px",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "rgba(0, 0, 0, 0.2)",
    },
  },
  column: { flexDirection: "column" },
  buttonText: {
    margin: "auto",
    height: "100%",
    display: "flex",
    alignItems: "center",
    color: "#2C699D",
    fontWeight: 600,
    cursor: "pointer",
    [theme.breakpoints.up("sm")]: {
      fontSize: 12,
    },
    [theme.breakpoints.up("md")]: {
      fontSize: 14,
    },
    [theme.breakpoints.up("xl")]: {
      fontSize: 16,
    },
  },
  button: {
    display: "flex",
    alignItems: "center",
    textAlign: "center",
    height: 33,
    marginTop: 10,
    marginBottom: 10,
    background: "#DDEAF5",
    backdropFilter: "blur(4px)",
    borderRadius: 6,
    [theme.breakpoints.up("md")]: {
      width: "calc(100vw - 800px)",
    },
    [theme.breakpoints.up("lg")]: {
      width: "calc(100vw - 950px)",
    },
    [theme.breakpoints.up("xl")]: {
      width: "calc(100vw - 1331px)",
    },
  },
  alert: {
    display: "flex",
    alignItems: "center",
    textAlign: "center",
    width: 265,
    height: 33,
    background: "#EF7E10",
    backdropFilter: "blur(4px)",
    borderRadius: 6,
    marginTop: 30,
    marginRight: 90,
    justifyContent: "center",
    [theme.breakpoints.up("sm")]: {
      width: 246,
    },
    [theme.breakpoints.up("md")]: {
      width: 266,
    },
    [theme.breakpoints.up("xl")]: {
      width: 399,
    },
  },
  alertText: {
    height: "100%",
    display: "flex",
    alignItems: "center",
    color: "#ffffff",
    fontWeight: 600,
    cursor: "default",
  },
  postit: {
    boxShadow: "rgba(0, 0, 0, 0.45) 0px 25px 20px -20px",
    width: 266,
    height: 184,
    background: "#DDEAF5",
    marginTop: 30,
    marginRight: 90,
    [theme.breakpoints.up("sm")]: {
      width: 246,
      height: 164,
    },
    [theme.breakpoints.up("md")]: {
      width: 266,
      height: 184,
    },
    [theme.breakpoints.up("xl")]: {
      width: 399,
      height: 276,
    },
  },
  postitTitle: {
    height: 30,
    width: "100%",
    backgroundColor: "#BEE1F4",
    display: "flex",
    justifyContent: "space-between",
  },
  postitTitleText: {
    color: "#2C699D",
    fontWeight: 600,
    margin: "auto",
    marginLeft: 15,
    marginRight: 15,
    display: "block",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  postitTitleButton: {
    color: "#2C699D",
    cursor: "pointer",
    fontWeight: 600,
    margin: "auto",
    marginRight: 25,
  },
  postitContent: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
  },
  postitContentText: {
    fontSize: 12,
  },
  postitContentTextSelected: {
    fontSize: 12,
    color: "#2C699D",
  },
}));

export default homeStyles;
