import React, { useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import homeStyles from "./styles";

const HomePage = ({ setPath }) => {
  const classes = homeStyles();

  const { pathname } = useLocation();

  useEffect(() => {
    setPath(pathname);
  }, []);
  return (
    <div className={classes.root}>
      <div className={classes.panel}>
        <div className={classes.column}>
          <div style={{ marginTop: 35, marginLeft: 30 }}>
            Seja bem vindo ao seu ambiente <b>Wellington</b>!
          </div>
          <div style={{ marginTop: 35, marginLeft: 30 }}>
            <div className={classes.button}>
              <div className={classes.buttonText}>Contas Pessoais</div>
            </div>
            <div className={classes.button}>
              <div className={classes.buttonText}>Compras e Sites</div>
            </div>
            <div className={classes.button}>
              <div className={classes.buttonText}>Seja um Representante</div>
            </div>
            <div className={classes.button}>
              <div className={classes.buttonText}>Ambiente do Contador</div>
            </div>
          </div>
          <div style={{ marginTop: 150, marginLeft: 35 }}>Último acesso em 13/04/2022 - 17:30hrs</div>
        </div>
        <div className={classes.column}>
          <div className={classes.alert}>
            <div className={classes.alertText}>Seu teste expira em 6 dias</div>
          </div>
          <div className={classes.postit}>
            <div className={classes.postitTitle}>
              <div className={classes.postitTitleText}>Ultimos Acessos:</div>
              <div className={classes.postitTitleButton}>X</div>
            </div>
            <div className={classes.postitContent}>
              <div className={classes.postitContentText}>Cadastro produto</div>
              <Link to="/terminais" style={{ textDecoration: "none", color: "#2C699D" }}>
                <div className={classes.postitContentTextSelected}>{"Checkout > Controle de Terminal"}</div>
              </Link>
              <div className={classes.postitContentText}>{"Cadastro de Cliente > Endereço"}</div>
              <div className={classes.postitContentText}>{"Cadastro de Cliente > Aba Funcionário"}</div>
            </div>
          </div>
          <div className={classes.postit}>
            <div className={classes.postitTitle}>
              <div className={classes.postitTitleText}>Lembretes:</div>
              <div className={classes.postitTitleButton}>X</div>
            </div>
            <div className={classes.postitContent}>
              <div className={classes.postitContentText}>
                Terminal de Pré-venda 3 não foi finalizado no dia 14/04.{" "}
                <u style={{ color: "#2C699D", cursor: "pointer" }}>
                  <b>Acessar</b>
                </u>
                <br />
                <br />
                Contas a pagar nro. 1236 - vencimento em atraso.{" "}
                <u style={{ color: "#2C699D", cursor: "pointer" }}>
                  <b>Acessar</b>
                </u>
              </div>
            </div>
          </div>
          <br />
        </div>
      </div>
    </div>
  );
};

export default HomePage;
