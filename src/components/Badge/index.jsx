import * as React from "react";
import { styled } from "@mui/material/styles";
import { Badge } from "@material-ui/core";

const StyledBadge = styled(Badge)(({ theme }) => ({
  "& .MuiBadge-badge": {
    backgroundColor: "#EF7E10",
    fontSize: 10,
    height: 18,
    width: 18,
    color: "white",
    right: 5,
    top: 5,
    border: `2px solid rgba(0,0,0,0)`,
    padding: "0 4px",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
  },
}));

export default StyledBadge;
