import { Popover } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import AppItem from "./components/AppItem";
import popoverStyles from "./styles";

const CustomPopover = ({ open, setOpen, anchor }) => {
  const classes = popoverStyles();
  const [anchorEl, setAnchorEl] = useState(null);

  useEffect(() => {
    if (open) {
      setAnchorEl(anchor);
    }
  }, [open]);
  return (
    <Popover
      className={classes.root}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "left",
      }}
      PaperProps={{
        style: {
          width: 280,
          height: 260,
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "space-between",
          overflowY: "auto",
          "&::-webkit-scrollbar": {
            width: "6px",
            height: "6px",
          },
          "&::-webkit-scrollbar-thumb": {
            backgroundColor: "rgba(0, 0, 0, 0.2)",
          },
        },
      }}
      anchorEl={anchorEl}
      open={open}
      onClose={() => setOpen(false)}
    >
      <AppItem text="Futura Mobile" selected />
      <AppItem text="Futura Mobile Vendas" />
      <AppItem text="Gerente Futura" />
      <AppItem text="Futura Mobile" />
      <AppItem text="Futura Mobile Vendas" />
      <AppItem text="Gerente Futura" />
      <AppItem text="Futura Mobile" />
      <AppItem text="Futura Mobile Vendas" />
      <AppItem text="Gerente Futura" />
    </Popover>
  );
};

export default CustomPopover;
