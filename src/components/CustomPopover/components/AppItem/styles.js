import { makeStyles } from "@material-ui/core";

const appItemStyles = makeStyles((theme) => ({
  root: {
    width: 77,
    height: 63,
    margin: 5,
    "&:hover": { background: "#F1F1F1", boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)" },
    borderRadius: 6,
    display: "flex",
    flexDirection: "column",
  },
  selected: { background: "#F1F1F1", boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)" },
  image: { margin: "auto" },
  text: {
    fontSize: 12,
    display: "block",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
}));

export default appItemStyles;
