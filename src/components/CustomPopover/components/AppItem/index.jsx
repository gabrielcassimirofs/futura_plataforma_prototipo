import { Tooltip } from "@material-ui/core";
import React from "react";
import popoverStyles from "../../styles";
import appItemStyles from "./styles";

const AppItem = ({ selected, text }) => {
  const classes = appItemStyles();
  return (
    <Tooltip title={text}>
      <div className={`${classes.root} ${selected ? classes.selected : ""}`}>
        <img src="/images/phone.png" alt="" className={classes.image} />
        <div className={classes.text}>{text}</div>
      </div>
    </Tooltip>
  );
};

export default AppItem;
