import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@material-ui/core";
import React from "react";

const PopupAlert = ({ open, handleClose }) => {
  return (
    <Dialog open={open} onClose={handleClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Deseja <b>desativar</b> este terminal?
        </DialogContentText>
      </DialogContent>
      <DialogActions style={{ justifyContent: "space-around" }}>
        <Button onClick={handleClose}>Sim</Button>
        <Button onClick={handleClose} autoFocus>
          Não
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default PopupAlert;
