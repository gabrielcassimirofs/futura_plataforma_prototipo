import React from "react";

import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import appBarStyles from "../../styles";

const CustomButton = (props) => {
  const classes = appBarStyles();
  return (
    <div
      className={props.selected ? classes.ButtonSelected : classes.Button}
      onClick={(e) => {
        props.setShowMenu(props.showIcon);
        props.select();
      }}
    >
      <div className={classes.ButtonText}>{props.children}</div>
      {props.showIcon && <KeyboardArrowDownIcon />}
    </div>
  );
};

export default CustomButton;
