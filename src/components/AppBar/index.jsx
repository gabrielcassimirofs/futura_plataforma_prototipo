import React, { useEffect, useState } from "react";
import { AppBar, IconButton, Toolbar } from "@material-ui/core";
import { Settings, Apps, KeyboardArrowDown, Search, Help, Notifications } from "@material-ui/icons";
import CustomMenu from "../Menu";
import appBarStyles from "./styles";
import CustomButton from "./components/CustomButton";
import StyledBadge from "../Badge";
import CustomPopover from "../CustomPopover";

const CustomAppBar = ({ path, isLogged }) => {
  const classes = appBarStyles();
  const [showMenu, setShowMenu] = useState(false);
  const [selected, setSelected] = useState("home");
  const [showPopover, setShowPopover] = useState(null);
  const [anchorEl, setAnchorEl] = useState(null);

  return (
    <div className={classes.grow}>
      {isLogged ? (
        <>
          <AppBar position="static" className={classes.AppBar}>
            <Toolbar className={classes.ToolBar}>
              <div className={classes.AppBarContent}>
                <div className={classes.title}>
                  <img src="/images/Logo-Portal---Plataforma-163x54.png" alt="" className={classes.imgLogo} />
                </div>
                <div className={classes.titleMobile}>
                  <img src="/images/Logovertical-Copia2.png" alt="" className={classes.imgLogo} />
                </div>
                <div className={classes.ComboBox}>
                  <div className={classes.ComboBoxText}>100 - Grupo Casa São João Ltda asda</div>
                  <KeyboardArrowDown style={{ marginTop: "auto", marginBottom: "auto" }} />
                </div>
                <IconButton
                  className={classes.IconButton}
                  onClick={(e) => {
                    setAnchorEl(e.currentTarget);
                    setShowPopover(true);
                  }}
                >
                  <Apps />
                </IconButton>
                <div style={{ flexGrow: 1 }} />
                <div className={classes.RightArea}>
                  <IconButton className={classes.SmallIconButton} size="small">
                    <StyledBadge badgeContent={17}>
                      <Notifications style={{ height: 24, width: 24 }} />
                    </StyledBadge>
                  </IconButton>
                  <IconButton className={classes.SmallIconButton} size="small">
                    <Help style={{ height: 20, width: 20 }} />
                  </IconButton>
                  <IconButton className={classes.SmallIconButton} size="small">
                    <Search style={{ height: 20, width: 20 }} />
                  </IconButton>
                  <IconButton className={classes.SmallIconButton} size="small">
                    <Settings style={{ height: 20, width: 20 }} />
                  </IconButton>
                  <div className={classes.UserIcon}>
                    <div className={classes.UserIconText}>W</div>
                  </div>
                </div>
              </div>
              <div className={classes.AppBarContent} style={{ border: "1px solid rgba(0, 0, 0, 0.2)" }} />
              <div className={classes.AppBarContent} style={{ marginLeft: 164 }}>
                <CustomButton setShowMenu={setShowMenu} select={() => setSelected("home")} showIcon selected={!path.includes("terminais")}>
                  Home
                </CustomButton>
                <CustomButton setShowMenu={setShowMenu} select={() => setSelected("cadastros")} showIcon>
                  Cadastros
                </CustomButton>
                <CustomButton setShowMenu={setShowMenu} select={() => setSelected("checkout")} showIcon selected={path.includes("terminais")}>
                  Gestão Checkout
                </CustomButton>
              </div>
            </Toolbar>
          </AppBar>
        </>
      ) : (
        <>
          <AppBar position="static" className={classes.AppBar} style={{ height: 100 }}>
            <Toolbar className={classes.ToolBar}>
              <div className={classes.AppBarContent}>
                <div className={classes.title}>
                  <img src="/images/Logo-Portal---Plataforma-163x54.png" alt="" className={classes.imgLogo} />
                </div>
                <div className={classes.titleMobile}>
                  <img src="/images/Logovertical-Copia2.png" alt="" className={classes.imgLogo} />
                </div>
                <div style={{ width: 25 }} />
                <div className={classes.landingText}>Funcionalidades</div>
                <div className={classes.landingText}>Planos e Preços</div>
                <div className={classes.landingText}>Fale Conosco</div>
                <div style={{ flexGrow: 1 }} />
                <div className={classes.landingButton}>
                  <div style={{ margin: 5 }}>Experimente grátis</div>
                </div>
                <div className={classes.landingButtonText}>Entrar</div>
              </div>
            </Toolbar>
          </AppBar>
        </>
      )}
      <CustomMenu open={showMenu} setOpen={setShowMenu} selected={selected} />
      <CustomPopover open={showPopover} setOpen={setShowPopover} anchor={anchorEl} />
    </div>
  );
};

export default CustomAppBar;
