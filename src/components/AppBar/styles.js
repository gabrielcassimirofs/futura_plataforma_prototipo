import { makeStyles } from "@material-ui/core";

const appBarStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  titleMobile: {
    display: "block",
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
    [theme.breakpoints.up("lg")]: {
      display: "none",
    },
  },
  AppBar: {
    backgroundColor: theme.palette.common.white,
    height: 110,
  },
  ToolBar: {
    height: "100%",
    flexDirection: "column",
  },
  AppBarContent: {
    display: "flex",
    margin: "auto",
    marginLeft: 160,
    marginRight: 160,
    width: "-webkit-fill-available",
  },
  Button: {
    color: "#000000",
    display: "flex",
    marginRight: 20,
    cursor: "pointer",
    borderBottom: "1px solid rgba(0,0,0,0)",
    "&:hover": { borderBottom: "1px solid #2C699D" },
  },
  ButtonSelected: {
    color: "#2C699D",
    display: "flex",
    marginRight: 20,
    cursor: "pointer",
    borderBottom: "1px solid #2C699D",
  },
  ButtonText: {
    fontSize: 14,
  },
  ComboBox: {
    display: "flex",
    width: 230,
    height: 30,
    fontWeight: "bold",
    textAlign: "center",
    marginTop: "auto",
    marginBottom: "auto",
    marginLeft: 30,
    marginRight: 14,
    color: "#000000",
    background: "#FFFFFF",
    border: "1px solid #EF7E10",
    boxSizing: "border-box",
    borderRadius: 4,
    cursor: "pointer",
  },
  ComboBoxText: {
    display: "block",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    width: 200,
    marginLeft: 5,
    marginRight: 5,
    marginTop: "auto",
    marginBottom: "auto",
  },
  IconButton: {
    marginTop: "auto",
    marginBottom: "auto",
    height: "auto",
    width: "auto",
  },
  SmallIconButton: {
    marginTop: "auto",
    marginBottom: "auto",
    height: "auto",
    width: "auto",
  },
  RightArea: {
    display: "flex",
  },
  UserIcon: {
    width: 38,
    height: 38,
    background: "#2C699D",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 40,
    marginTop: "auto",
    marginBottom: "auto",
  },
  UserIconText: {
    fontWeight: 700,
    fontSize: 18,
    textAlign: "center",
    color: "#FFFFFF",
    paddingTop: 7,
  },
  landingText: {
    fontWeight: 300,
    fontSize: 16,
    color: "#000000",
    margin: "auto",
    marginLeft: 15,
    marginRight: 15,
    cursor: "pointer",
  },
  landingButton: {
    margin: "auto",
    width: 217,
    height: 33,
    background: "#EF7E10",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    borderRadius: 6,
    textAlign: "center",
    cursor: "pointer",
  },
  landingButtonText: {
    margin: "auto",
    marginLeft: 15,
    marginRight: 15,
    textAlign: "center",
    textDecorationLine: "underline",
    color: "#2E699D",
    fontSize: 16,
    cursor: "pointer",
  },
}));

export default appBarStyles;
