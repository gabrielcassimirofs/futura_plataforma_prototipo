import { Popover } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import MenuCadastros from "./components/cadastros";
import MenuCheckout from "./components/checkout";
import MenuHome from "./components/home";
import menuStyles from "./styles";

const CustomMenu = ({ id, open, setOpen, selected }) => {
  const classes = menuStyles();
  //#region - Functions

  const handleMenuClose = () => setOpen(false);

  //#endregion - Functions

  return (
    <Popover
      id={id ?? ""}
      style={{ display: "flex" }}
      PaperProps={{
        style: {
          width: 900,
          height: 190,
        },
      }}
      open={open}
      onClose={handleMenuClose}
      anchorReference="anchorPosition"
      anchorPosition={{ top: 110, left: 180 }}
      keepMounted
    >
      {selected == "home" && <MenuHome handleMenuClose={handleMenuClose} />}
      {selected == "cadastros" && <MenuCadastros handleMenuClose={handleMenuClose} />}
      {selected == "checkout" && <MenuCheckout handleMenuClose={handleMenuClose} />}
    </Popover>
  );
};

export default CustomMenu;
