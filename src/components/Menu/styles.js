import { makeStyles } from "@material-ui/core";

const menuStyles = makeStyles((theme) => ({
  Menu: {
    display: "flex",
    width: "100%",
    height: "100%",
  },
  SectionMenu: {
    width: 150,
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
  },
  MenuTitle: {
    fontWeight: 700,
    fontSize: 13,
    paddingLeft: 4,
  },
  LineDivider: {
    height: 160,
    width: 0,
    border: "1px solid rgba(0, 0, 0, 0.2)",
    marginTop: "auto",
    marginBottom: "auto",
  },
  Button: {
    fontSize: 12,
    height: 20,
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "rgba(44, 105, 157, 0.1)",
      color: "#2C699D",
    },
  },
  ButtonSelected: {
    cursor: "pointer",
    fontSize: 12,
    height: 20,
    backgroundColor: "rgba(44, 105, 157, 0.1)",
    color: "#2C699D",
  },
  ButtonBlocked: {
    cursor: "not-allowed",
    fontSize: 12,
    height: 20,
    color: "rgba(0, 0, 0, 0.4)",
  },
  ButtonText: {
    marginTop: "auto",
    marginBottom: "auto",
    height: "100%",
    paddingTop: 2,
    paddingLeft: 4,
    display: "block",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  ButtonTextSelected: {
    marginTop: "auto",
    marginBottom: "auto",
    height: "100%",
    paddingTop: 2,
    paddingLeft: 4,
    fontWeight: "bold",
    display: "block",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  Image: { width: 190, height: "fit-content", margin: "auto" },
}));

export default menuStyles;
