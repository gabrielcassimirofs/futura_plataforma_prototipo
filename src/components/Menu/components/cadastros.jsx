import React from "react";
import menuStyles from "../styles";

const MenuCadastros = ({ handleMenuClose }) => {
  const classes = menuStyles();

  return (
    <div className={classes.Menu}>
      <div className={classes.SectionMenu}>
        <div className={classes.MenuTitle}>Pessoa Jurídica/Física</div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Clientes</div>
        </div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Fornecedores</div>
        </div>
        <br />
        <div className={classes.MenuTitle}>Funcionários</div>
        <div onClick={handleMenuClose} className={classes.ButtonSelected}>
          <div className={classes.ButtonTextSelected}>Usuários</div>
        </div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Transportadoras</div>
        </div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Contabilidades</div>
        </div>
      </div>
      <div className={classes.LineDivider} />
      <div className={classes.SectionMenu}>
        <div className={classes.MenuTitle}>Estoque</div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Produtos</div>
        </div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Categorias</div>
        </div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Unidades</div>
        </div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Grade</div>
        </div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>NCMs</div>
        </div>
      </div>
      <div className={classes.LineDivider} />
      <div className={classes.SectionMenu}>
        <div className={classes.MenuTitle}>Financeiro</div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Tabelas de Preços</div>
        </div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Meios de Pagamentos</div>
        </div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Conta Corrente</div>
        </div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Banco</div>
        </div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Plano de Contas</div>
        </div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Cartão - Credenciadoras</div>
        </div>
      </div>
      <div className={classes.LineDivider} />
      <div className={classes.SectionMenu} style={{ width: 90 }}>
        <div className={classes.MenuTitle} style={{ width: 90 }}>
          Outros
        </div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Funções</div>
        </div>
      </div>
      <img src="/images/BannerPlataforma.png" alt="" className={classes.Image} />
    </div>
  );
};

export default MenuCadastros;
