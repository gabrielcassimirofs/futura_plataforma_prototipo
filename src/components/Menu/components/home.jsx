import React from "react";
import { Link } from "react-router-dom";
import menuStyles from "../styles";

const MenuHome = ({ handleMenuClose }) => {
  const classes = menuStyles();

  return (
    <div className={classes.Menu}>
      <div className={classes.SectionMenu}>
        <div className={classes.MenuTitle}>Home</div>
        <div onClick={handleMenuClose} className={classes.ButtonSelected}>
          <Link to="/" style={{ textDecoration: "none", color: "#2C699D" }}>
            <div className={classes.ButtonTextSelected}>Dashboard</div>
          </Link>
        </div>
        <br />
        <div className={classes.MenuTitle}>Configurações</div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Parâmetros</div>
        </div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Configurações de Usuários</div>
        </div>
      </div>
      <div className={classes.LineDivider} />
      <div className={classes.SectionMenu}>
        <div className={classes.MenuTitle}>Minha Conta</div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Grupo de Empresas</div>
        </div>
        <br />
        <div className={classes.MenuTitle}>Financeiro</div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Mensalidades</div>
        </div>
      </div>
      <div className={classes.LineDivider} />
      <div className={classes.SectionMenu}>
        <div className={classes.MenuTitle}>Loja de Aplicativos</div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Comprar</div>
        </div>
        <br />
        <div className={classes.MenuTitle}>Planos</div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Gerenciar Assinatura</div>
        </div>
        <br />
        <div className={classes.MenuTitle}>Certificado Digital</div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Administrar</div>
        </div>
      </div>
      <div className={classes.LineDivider} />
      <div className={classes.SectionMenu} style={{ width: 90 }}>
        <div className={classes.MenuTitle} style={{ width: 90 }}>
          Relatórios
        </div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Relatórios</div>
        </div>
      </div>
      <img src="/images/BannerPlataforma.png" alt="" className={classes.Image} />
    </div>
  );
};

export default MenuHome;
