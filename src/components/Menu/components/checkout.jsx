import React from "react";
import { Link } from "react-router-dom";
import menuStyles from "../styles";

const MenuCheckout = ({ handleMenuClose }) => {
  const classes = menuStyles();

  return (
    <div className={classes.Menu}>
      <div className={classes.SectionMenu}>
        <div className={classes.MenuTitle}>Home</div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Dashboard</div>
        </div>
        <br />
        <div className={classes.MenuTitle}>Configurações</div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Parâmetros</div>
        </div>
      </div>
      <div className={classes.LineDivider} />
      <div className={classes.SectionMenu}>
        <div className={classes.MenuTitle}>Estoque</div>
        <div onClick={handleMenuClose} className={classes.ButtonSelected}>
          <Link to="/terminais" style={{ textDecoration: "none", color: "#2C699D" }}>
            <div className={classes.ButtonTextSelected}>Terminais</div>
          </Link>
        </div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>SATs</div>
        </div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>TEF</div>
        </div>
        <div onClick={handleMenuClose} className={classes.ButtonBlocked}>
          <div className={classes.ButtonText}>Balanças</div>
        </div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>NFCe</div>
        </div>
      </div>
      <div className={classes.LineDivider} />
      <div className={classes.SectionMenu}>
        <div className={classes.MenuTitle}>Movimentos</div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Caixa</div>
        </div>
        <div onClick={handleMenuClose} className={classes.Button}>
          <div className={classes.ButtonText}>Vendas</div>
        </div>
      </div>
      <div className={classes.LineDivider} />
      <div className={classes.SectionMenu} style={{ width: 90 }}>
        <div className={classes.MenuTitle} style={{ width: 90 }}>
          Relatórios
        </div>
      </div>
      <img src="/images/BannerPlataforma.png" alt="" className={classes.Image} />
    </div>
  );
};

export default MenuCheckout;
